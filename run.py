import configparser
import os
import re
import subprocess
from openpyxl import load_workbook


settings = configparser.ConfigParser()
settings.read('config.ini')
wb = load_workbook(filename='results.xlsx')
ws = wb['results']
row = 1

for root, dirs, files in os.walk(settings.get("MainConfig", "InputDirectory")):
    for f in files:
        if f.endswith(('.py', '.cc')):
            results = subprocess.check_output("cloc " + os.path.join(root, f), shell=True)
            info = re.findall(r"\n([A-Za-z+]*)\s*\d+\s*(\d+)\s*(\d+)\s*(\d+)", results, re.M)
            try:
                data = info[0]
            except IndexError:
                continue
            row += 1
            ws.cell(row, 1, os.path.abspath(root))
            ws.cell(row, 2, f)
            ws.cell(row, 3, data[0])
            ws.cell(row, 4, int(data[3]))
            ws.cell(row, 5, int(data[2]))
            ws.cell(row, 6, int(data[1]))

wb.save(filename='results.xlsx')
print 'Done'
